﻿using System;
using System.Data.SqlClient;


namespace DBQueryLibraryAdoNet
{
    public static class DBQuery
    {
        private static string sqlConnectionString = "Data Source=elma-td1;Initial Catalog=ELMA_base;User=elma;Password=P@ssw0rd;";
        public static double PriceToDate(string date1, Int32 idArticle)
        {
            double price = 0;
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {

                connection.Open();
                string selectQuery = "SELECT TOP (1) [Id],[IdArticle],[DateIzm],[PriceArticle] " +
                                     String.Format("FROM [Views].[dbo].[otus_Price] where IdArticle = {0} and DateIzm<=convert(datetime,'{1}',104) order by DateIzm desc", idArticle, date1);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return -1;
                    while (reader.Read())
                    {

                        price = Convert.ToDouble(reader.GetValue(3));

                    }
                    reader.Close();
                };
                connection.Close();
            }
            return price;
        }
        public static bool IsThereArticle(string date1, Int32 idArticle, Int32 amount)
        {
            double amountFound = 0;
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {

                connection.Open();
                string selectQuery = "SELECT isnull(sum(Amount),0)  " +
                                     String.Format("FROM [Views].[dbo].[otus_Store] where IdArticle = {0} and DateInStore<=convert(datetime,'{1}',104) ", idArticle, date1);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        amountFound = Convert.ToDouble(reader.GetValue(0));

                    }
                    reader.Close();
                };
                connection.Close();
            }
            return amountFound >= amount;
        }

        public static double TotalOrder(Int32 idOrder)
        {
            double total = 0;
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = "SELECT isnull(sum([Amount]*[Views].[dbo].[otus_Price_Article](DateOrder,IdArticle)),0) " +
                                     "FROM [Views].[dbo].[otus_Order] " +
                                     String.Format("WHERE NOrder = {0} ", idOrder);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        total = Convert.ToDouble(reader.GetValue(0));

                    }
                    reader.Close();
                };
                connection.Close();
            }
            return total;
        }
        public static void ToStore(string date1, int idArticle, int amount)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string insertQuery = String.Format("insert into [Views].[dbo].[otus_Store] values(convert(datetime,'{0}',104),{1},{2})", date1, idArticle, amount);
                using (SqlCommand insertCommand = new SqlCommand(insertQuery, connection))
                {
                    var res = insertCommand.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public static string StoreADay(string date1)
        {
            string resultString = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = "select a.NameItem,sum(s.Amount) Amount from [Views].[dbo].[otus_Article] a " +
                                     "inner join [Views].[dbo].[otus_Store] s on a.id = s.idArticle " +
                                     String.Format("where s.DateInStore<=convert(datetime,'{0}',104) ", date1) +
                                     "group by a.NameItem ";
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows) return "There is none";
                    while (reader.Read())
                    {

                        resultString += reader.GetValue(0).ToString() + ": " + reader.GetValue(1).ToString() + "\r\n";

                    }
                    reader.Close();
                };
                connection.Close();
                return "На " + date1 + " на складе: \r\n" + resultString;
            }
        }

        public static string BestClient()
        {
            string client = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {

                connection.Open();
                string selectQuery = "SELECT top(1) c.FirstName+' '+c.LastName ,sum([Amount]*[Views].[dbo].[otus_Price_Article](DateOrder,IdArticle)) " +
                                     "FROM [Views].[dbo].[otus_Order] o " +
                                     "inner join [Views].[dbo].[otus_Client] c on o.IdClient = c.Id " +
                                     "group by c.FirstName,c.LastName " +
                                     "order by 2 desc ";
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return "There is none";
                    while (reader.Read())
                    {

                        client = reader.GetValue(0).ToString();

                    }
                    reader.Close();
                }
                ;
                connection.Close();
            }
            return client;
        }
    }
}
