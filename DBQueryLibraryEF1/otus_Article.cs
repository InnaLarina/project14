﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBQueryLibraryEF1
{
    class otus_Article
    {
        public int Id { get; set; }
        public string NameItem { get; set; }
        public string Color { get; set; }
        public string Producer { get; set; }
    }
}
