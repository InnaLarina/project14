﻿using System.Data.Entity;

namespace DBQueryLibraryEF1
{
    class otus_OrderContext:DbContext
    {
        public otus_OrderContext() : base("DBConnection")
        { }
        public DbSet<otus_Order> otus_Orders { get; set; }
    }
}
