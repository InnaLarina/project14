﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBQueryLibraryEF1
{
    class otus_Order
    {
        public int Id { get; set; }
        public int NOrder { get; set; }
        public DateTime DateOrder { get; set; }
        public int idArticle { get; set; }
        public int Amount { get; set; }
        public int idClient { get; set; }
    }
}
